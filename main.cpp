#include <stdio.h>
#include <stdlib.h>
#include <windows.h>
#include <tlhelp32.h>
#include <math.h>

#include "mumble_plugin.h"

HANDLE h;
BYTE *posptr;
BYTE *frontptr;
static DWORD getProcess(const char *exename) {
	PROCESSENTRY32 pe;
	DWORD pid = 0;

	pe.dwSize = sizeof(pe);
	HANDLE hSnap = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
	if (hSnap != INVALID_HANDLE_VALUE) {
		BOOL ok = Process32First(hSnap, &pe);

		while (ok) {
			if (strcmp(pe.szExeFile, exename)==0) {
				pid = pe.th32ProcessID;
				break;
			}
			ok = Process32Next(hSnap, &pe);
		}
		CloseHandle(hSnap);
	}
	return pid;
}

static bool peekProc(VOID *base, VOID *dest, SIZE_T len) {
	SIZE_T r;
	BOOL ok=ReadProcessMemory(h, base, dest, len, &r);
	return (ok && (r == len));
}

static void about(HWND h) {
	::MessageBox(h, "Version: 0.8b \nAuthor: repeat\nhttp://pereulok.net.ru/", "Mumble OFP Plugin", MB_OK);
}

static int fetch(float *pos, float *front, float *top) {

    BYTE *mod;
    mod = NULL;
    ReadProcessMemory(h, (VOID *)0x007831C0, &mod, 1, 0);
    if (mod == (BYTE *)0x04)
        return false;

    for (int i=0;i<3;i++)
		pos[i]=front[i]=top[i]=0.0;

	BOOL ok = true;

        ReadProcessMemory(h, (VOID *)0x0079F8D0, &mod, 4, 0);
        posptr = mod + 0x07A8;
        ReadProcessMemory(h, posptr, &mod, 4, 0);
        posptr = mod + 0x08;
        ReadProcessMemory(h, posptr, &mod, 4, 0);
        posptr = mod + 0x40;
        frontptr = mod + 0x34; //проверить соответствие осей.

	ok = ok && peekProc(posptr, pos, 12);
	ok = ok && peekProc(frontptr, front, 12);
	if (! ok)
		return false;

    top[1]=1.0;

	return true;
}

static int trylock() {

	h = NULL;
//    posptr = frontptr = NULL;

	DWORD pid=getProcess("FLASHPOINTRESISTANCE.EXE");
	if (!pid)
		return false;

	h=OpenProcess(PROCESS_VM_READ, false, pid);
	if (!h)
		return false;

/*
    BYTE *mod;
    mod = NULL;
    ReadProcessMemory(h, (VOID *)0x10041B70, &mod, 1, 0);
    if (mod != (BYTE *)0x02)
    {
        ReadProcessMemory(h, (VOID *)0x1052F2A8, &mod, 4, 0);
        posptr = mod + 0x08;
        ReadProcessMemory(h, posptr, &mod, 4, 0);
        posptr = mod + 0x40;
        frontptr = mod + 0x34; //проверить соответствие осей.

*/
        float pos[3], front[3], top[3];
        if (fetch(pos, front, top))
            return true;

        CloseHandle(h);
        h = NULL;
//    }
	return false;
}

static void unlock() {
	if (h) {
		CloseHandle(h);
		h = NULL;
	}
	return;
}

static MumblePlugin ofpplug = {
	MUMBLE_PLUGIN_MAGIC,
	L"OFP 1.96",
	L"Mumble OFP Plugin",
	about,
	NULL,
	trylock,
	unlock,
	fetch
};

extern "C" __declspec(dllexport) MumblePlugin *getMumblePlugin() {
	return &ofpplug;
}

/*
int main()
{

	DWORD pid=getProcess("FLASHPOINTRESISTANCE.EXE");
	if (!pid)
		return false;

	h=OpenProcess(PROCESS_VM_READ, false, pid);
    POINT cp;
	BOOL ok = true;
while (1) {
	ReadProcessMemory(h, (LPVOID *)0x0012DA28, &posptr, 4, 0);
	printf("%x \n", posptr);
//	GetCursorPos(&cp);
    //if (cp.x > 1024/2) { pos[1] = cp.x/1023.0*200-100; }
    //if (cp.x <= 1024/2) { pos[1] = cp.x/1023.0*20-5; }
//    printf("%f\n",cp.x/1023.0*200-100);
}
    return 0;
}
*/
